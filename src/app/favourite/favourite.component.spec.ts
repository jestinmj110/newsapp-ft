import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { FavouriteComponent } from './favourite.component';

describe('FavouriteComponent', () => {
  let component: FavouriteComponent;
  let fixture: ComponentFixture<FavouriteComponent>;

  beforeEach(async () => {
     TestBed.configureTestingModule({
      imports: [ HttpClientModule, FormsModule,RouterTestingModule],
      declarations: [ FavouriteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

   // test to check ngOnInit method existence
   it('ngOnInit() should exists', () => {
    expect(component.ngOnInit).toBeTruthy();
  });

  // test to check getFavNewsList method existence
  it('getFavNewsList() should exists', () => {
    expect(component.getFavNewsList).toBeTruthy();
  });

  // test to check removeNews method existence
  it('removeNews() should exists', () => {
    expect(component.removeNews).toBeTruthy();
  });



});