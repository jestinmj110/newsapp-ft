import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Favourite } from '../favourite';
import { ServiceService } from '../Service/service.service';
import { User } from '../user';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {
  user= new User();
  url=""
  msg=""
  userid="";
  news: any[]=[];
  favlist: Favourite[] = [];
  favNews= new Favourite();
  constructor(private _service : ServiceService, private _router : Router) { }

  ngOnInit(){
    this.getFavNewsList();
    this.userid=this._service.getUser();
    this.url="http://localhost:9003/files/"+this.userid;
  }

  getFavNewsList(){
    this._service.getFavList().subscribe(data=>{
      this.favlist = data;
    });
  }

  removeNews(id){
    
    if(this._service.getUser()==""){
      alert("Login With Proper Details")
      return
    }
    this._service.setNewsId(id);
    this._service.removefavlistFromRemote().subscribe(
      data=>{
      alert("News Removed successfully");
    },error =>{
      this.msg= "News not present in Your list.Please Reload"
      alert(this.msg)
      console.log("Exeption Occuerd");
    });
    
  }
}
