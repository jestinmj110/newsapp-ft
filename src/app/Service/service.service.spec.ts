import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ServiceService } from './service.service';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { User } from '../user';
import { RouterTestingModule } from '@angular/router/testing';

describe('ServiceService', () => {
  let service: ServiceService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,HttpClientModule,FormsModule,RouterTestingModule],
      providers:[ServiceService]
    });
    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.inject(ServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  
  afterEach( () => {
    httpMock.verify();
 });
});
