import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Favourite } from '../favourite';
import { Image } from '../image';
import { Recommended } from '../recommended';

import { User } from '../user';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  
  user=""
  newsid=0;
  userID=0;
  usermail=""
  category=0;
  baseUrl="";
  baseURL="";
  constructor(private _http : HttpClient) { }

  public loginUserFromRemote(user :User):Observable<any>{
    
    return this._http.post<any>("http://localhost:9000/loginuser",user)
 }

 public registerUserFromRemote(user :User):Observable<any>{
   return this._http.post<any>("http://localhost:9000/registeruser",user)
 }

 public getIdFromRemote():Observable<any>{
   this.baseUrl="http://localhost:9000/userid/"+this.getUser()
   return this._http.get<any>(this.baseUrl)
 }

 public addtofavlistFromRemote(favlist :Favourite):Observable<any>{
  return this._http.post<any>("http://localhost:9002/addtolist",favlist)
}

getFavList(): Observable<Favourite[]>{
 
  this.baseURL="http://localhost:9002/favlist/"+this.getUserId();
  return this._http.get<Favourite[]>(`${this.baseURL}`);
} 
public removefavlistFromRemote(){
  let url=`http://localhost:9002/favlist/`+this.getNewsId();
  return this._http.delete(url);
}
getRecommendedList():Observable<Recommended[]> {
  this.baseURL="http://localhost:9002/recommended"
  return this._http.get<Recommended[]>(`${this.baseURL}`);
}
 setUser(data){
  this.user=data;
}
getUser(){
  return this.user;
}
setUserId(data){
  this.userID=data;
}
getUserId(){
  return this.userID;
}
setCategory(data){
  this.category=data;
}
getCategory(){
  return this.category;
}
setNewsId(data){
  this.newsid=data;
}
getNewsId(){
  return this.newsid;
}
// getUserimage():Observable<Image[]>{
//   return this._http.get<Image[]>("http://localhost:8080/files/"+this.getUser());
// }
getNewsList(): Observable<any>{
  return this._http.get(`https://newsapi.org/v2/everything?q=apple&from=2021-12-18&to=2021-12-18&sortBy=popularity&apiKey=cf2509964da04a9f831feb83d6273862`);
}
getNewsListOne(): Observable<any>{
  return this._http.get(`https://newsapi.org/v2/top-headlines?country=us&apiKey=cf2509964da04a9f831feb83d6273862`);
}
getNewsListTwo(): Observable<any>{
  return this._http.get(`https://newsapi.org/v2/top-headlines?country=in&apiKey=cf2509964da04a9f831feb83d6273862`);
}
getNewsListThree(): Observable<any>{
  return this._http.get(`https://newsapi.org/v2/top-headlines?country=in&category=business&apiKey=cf2509964da04a9f831feb83d6273862`);
}
getNewsListFour(): Observable<any>{
  return this._http.get(`https://newsapi.org/v2/top-headlines?country=in&category=general&apiKey=cf2509964da04a9f831feb83d6273862`);
}
getNewsListFive(): Observable<any>{
  return this._http.get(`https://newsapi.org/v2/top-headlines?country=in&category=health&apiKey=cf2509964da04a9f831feb83d6273862`);
}
getNewsListSix(): Observable<any>{
  return this._http.get(`https://newsapi.org/v2/top-headlines?country=in&category=science&apiKey=cf2509964da04a9f831feb83d6273862`);
}
getNewsListSeven(): Observable<any>{
  return this._http.get(`https://newsapi.org/v2/top-headlines?country=in&category=sports&apiKey=cf2509964da04a9f831feb83d6273862`);
}
getNewsListEight(): Observable<any>{
  return this._http.get(`https://newsapi.org/v2/top-headlines?country=in&category=technology&apiKey=cf2509964da04a9f831feb83d6273862`);
}
getNewsListNine(): Observable<any>{
  return this._http.get(`https://newsapi.org/v2/top-headlines?country=in&category=entertainment&apiKey=cf2509964da04a9f831feb83d6273862`);
}
getNewsListTen(): Observable<any>{
  return this._http.get(`https://newsapi.org/v2/everything?q=tesla&from=2021-11-19&sortBy=publishedAt&apiKey=cf2509964da04a9f831feb83d6273862`);
}

}