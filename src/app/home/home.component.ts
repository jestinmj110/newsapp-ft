import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Favourite } from '../favourite';
import { Image } from '../image';
import { ServiceService } from '../Service/service.service';
import { User } from '../user';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  user= new User();
  url=""
  userid="";
  news: any[]=[];
  
  constructor(private _service : ServiceService, private _router : Router){}

  ngOnInit(): void {
    this.userid=this._service.getUser();

    this.url="http://localhost:9003/files/"+this.userid;
    this._service.getIdFromRemote().subscribe(data=>{
      this._service.setUserId(data);
    })
  }
  
  getNews(category){
    this._service.setCategory(category);
    this._router.navigate(['/news']);
  }
  
}
