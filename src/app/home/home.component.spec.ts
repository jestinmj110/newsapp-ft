import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule, FormsModule,RouterTestingModule],
      declarations: [ HomeComponent ]
      
    })
    .compileComponents();
  });
  
  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

   // test to check ngOnInit method existence
   it('ngOnInit() should exists', () => {
    expect(component.ngOnInit).toBeTruthy();
  });

   // test to check getNews method existence
   it('getNews() ture', () => {
    
  });

  


});