import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ServiceService } from '../Service/service.service';


import { SignupComponent } from './signup.component';

describe('SignupComponent', () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;
  let serviceService: ServiceService;
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule, FormsModule, RouterTestingModule],
      declarations: [ SignupComponent ],
      providers: [ServiceService]
    })
    .compileComponents();
    serviceService = TestBed.inject(ServiceService);
    spyOn(serviceService,'registerUserFromRemote').and.returnValue(of(''));
  
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // test to check ngOnInit method existence
  it('ngOnInit() should works', () => {
    expect(component.ngOnInit).toBeTruthy();
  });

  // test to check onSubmit method existence
  it('signUp() should be there', () => {
    expect(component.signUp).toBeTruthy();
  });

  // test to check signUp is verifying form is valid or not
//   it('signUp() should return success ', () => {
//     component.user.emailId ='axey@gmail.com';
//     component.user.password='axey';
//     component.user.mobile=987654321;
//     component.user.username='axey';
//     //component.image.url="https://www.pngitem.com/pimgs/m/265-2651779_batman-png-pic-batman-cartoon-images-hd-transparent.png";
//     component.signUp();
//     expect(component.msg).toEqual('Registration successful');
// });
   
 // test to check signup without data
it('signUp() should return error message ', () => {
  component.signUp();
  expect(component.message).toEqual('register failed');
});

// test to check signup with some data
it('signUp() should return error message while input some data ', () => {
  component.user.emailId='test@gmail.com';
  component.signUp();
  expect(component.message).toEqual('register failed');
});

});