import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { User } from '../user';
import { Router } from '@angular/router';
import { ServiceService } from '../Service/service.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  user= new User();
  msg='';
  id="";
  message='';
  selectedFile!: File;
  
  imgUrl="https://i.pinimg.com/736x/41/71/44/4171449ab62bcdc64872ed4b0b9edf64.jpg";
  baseURL: any;
  constructor( private _service: ServiceService, private httpClient: HttpClient, private _router : Router) { }

  ngOnInit() {
  }
  public onFileChanged(event) {
    //Select File
    this.selectedFile = event.target.files[0];
    
    console.log(this.selectedFile)
  } 

  onUpload() {
    console.log(this.user.emailId);
    
    const uploadImageData = new FormData();

    uploadImageData.append('file', this.selectedFile);
    
   
    this.baseURL = "http://localhost:9003/upload/"+this.user.emailId;
    this.httpClient.post(`${this.baseURL}`, uploadImageData, { observe: 'response' })
      .subscribe((response) => {
        if (response.status === 200) {
          this.message = 'Image uploaded successfully';
          return
        } else {
          this.message = 'Image not uploaded successfully';
          alert(this.message);  
        }
      }
      
      );


  }

  signUp(){
    if(this.user.emailId==null || this.user.password==null || this.user.username ==null){
      console.log("null");
      this.message="register failed";
      alert("Please enter Your details")
      return
    }
    if(this.user.password!=this.user.cpassword){
      this.message="Password Mismatch"
      return
    }
    if(this.selectedFile==null){
      this.message="Please Upload Profile picture"
      return
    }
    this.onUpload();
    console.log(this.user);
    this._service.registerUserFromRemote(this.user).subscribe(
      data =>{
        console.log("response recieved"); 
        
         this.msg="Registration successful";
        alert("Registration successful");
        this._router.navigate(['/login']);

       },
       error=>{
         this.message="Register failed";
         console.log("exception Occured");
         this.msg=error.error;
         
       }
     );
    
  }

}
