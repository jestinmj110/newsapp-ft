import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ServiceService } from '../Service/service.service';
import { RecommendedComponent } from './recommended.component';


describe('RecommendComponent', () => {
  let component: RecommendedComponent;
  let fixture: ComponentFixture<RecommendedComponent>;
  let serviceService: ServiceService;
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule, FormsModule,RouterTestingModule],
      declarations: [ RecommendedComponent ],
      providers: [ServiceService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecommendedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

   // test to check ngOnInit method existence
   it('ngOnInit() should exists', () => {
    expect(component.ngOnInit).toBeTruthy();
  });

    // test to check getRecNewsList method existence
   it('getRecNewsList() should exists', () => {
    expect(component.getRecNewsList).toBeTruthy();
  });

});