import { Component, OnInit } from '@angular/core';
import { Recommended } from '../recommended';
import { ServiceService } from '../Service/service.service';

@Component({
  selector: 'app-recommended',
  templateUrl: './recommended.component.html',
  styleUrls: ['./recommended.component.css']
})
export class RecommendedComponent implements OnInit {
  recommended: Recommended[]=[];
  userid="";
  url=""
  constructor(private _service : ServiceService) { }

  ngOnInit(): void {
    this.getRecNewsList();
    this.userid=this._service.getUser();
    this.url="http://localhost:9003/files/"+this.userid;
  }
  getRecNewsList(){
    this._service.getRecommendedList().subscribe(data=>{
      this.recommended = data;
    });
  }

}
