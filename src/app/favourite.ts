export class Favourite {
    favid!: number;
    userid!: number;
    title!: string;
    description!: string;
    url!: string;
    urlToImage!: string;
    
}
