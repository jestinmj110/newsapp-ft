import { Component, OnInit } from '@angular/core';
import { Favourite } from '../favourite';
import { Image } from '../image';
import { ServiceService } from '../Service/service.service';
import { User } from '../user';
import { from, Observable } from 'rxjs';
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  selectedFile!: File;
  message!: string;
 
  user= new User();
  url=""
  userid="";
  news: any[]=[];
  category!: number;
  favlist= new Favourite();
  msg="";
  constructor(private _service : ServiceService){}

  ngOnInit(): void {
    // this.getUserIamge();
    this.userid=this._service.getUser();
    this.getNews()
    
    console.log(this._service.getUser);
    this.url="http://localhost:9003/files/"+this.userid;
  }
  
  addtoList(title, description, url, urlToImage){
    if(this._service.getUser()==""){
      alert("Login With Proper Details")
      return
    }
    console.log(this.userid);
    
    this._service.getIdFromRemote().subscribe(data=>{
    console.log(data)
    this.favlist.title=title;
    this.favlist.description=description;
    this.favlist.url=url;
    this.favlist.urlToImage=urlToImage;
    this.favlist.userid=data;
    this._service.addtofavlistFromRemote(this.favlist).subscribe(data=>{
        alert("Added to Your List");
      
      },
      error => {
        this.msg= "News is Already in your List"
        alert(this.msg)
        console.log("Exeption Occuerd");
        
      })
    })
  
  }



  getNews(){
    this.category=this._service.getCategory();
    
    if(this.category==0){
      this._service.getNewsList().subscribe(data=>{
        console.log(data);
        this.news = data.articles;
      });
    }

    if(this.category==1){
      this._service.getNewsListOne().subscribe(data=>{
        console.log(data);
        this.news = data.articles;
      });
    }
    if(this.category==2){
      this._service.getNewsListTwo().subscribe(data=>{
        console.log(data);
        this.news = data.articles;
      });
    }
    if(this.category==3){
      this._service.getNewsListThree().subscribe(data=>{
        console.log(data);
        this.news = data.articles;
      });
    }
    if(this.category==4){
      this._service.getNewsListFour().subscribe(data=>{
        console.log(data);
        this.news = data.articles;
      });
    }
    if(this.category==5){
      this._service.getNewsListFive().subscribe(data=>{
        console.log(data);
        this.news = data.articles;
      });
    }
    if(this.category==6){
      this._service.getNewsListSix().subscribe(data=>{
        console.log(data);
        this.news = data.articles;
      });
    }
    if(this.category==7){
      this._service.getNewsListSeven().subscribe(data=>{
        console.log(data);
        this.news = data.articles;
      });
    }
    if(this.category==8){
      this._service.getNewsListEight().subscribe(data=>{
        console.log(data);
        this.news = data.articles;
      });
    }
    if(this.category==9){
      this._service.getNewsListNine().subscribe(data=>{
        console.log(data);
        this.news = data.articles;
      });
    }
    if(this.category==10){
      this._service.getNewsListTen().subscribe(data=>{
        console.log(data);
        this.news = data.articles;
      });
    }
    
  }

}
