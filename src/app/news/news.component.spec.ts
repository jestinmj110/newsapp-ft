import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ServiceService } from '../Service/service.service';

import { NewsComponent } from './news.component';

describe('NewsComponent', () => {
  let component: NewsComponent;
  let fixture: ComponentFixture<NewsComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [ NewsComponent ],
      imports: [HttpClientTestingModule,HttpClientModule,FormsModule,RouterTestingModule],
      providers:[ServiceService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

   // test to check ngOnInit method existence
   it('ngOnInit() should exists', () => {
    expect(component.ngOnInit).toBeTruthy();
  });

   // test to check addtoList method existence
   it('addtoList() should exists', () => {
    expect(component.addtoList).toBeTruthy();
  });

  // test to check getNews method existence
  it('getNews() should exists', () => {
    expect(component.getNews).toBeTruthy();
  });
  

});
