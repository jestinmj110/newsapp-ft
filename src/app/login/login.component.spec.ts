import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { HomeComponent } from '../home/home.component';
import { ServiceService } from '../Service/service.service';


import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let serviceService: ServiceService;
  let home:HomeComponent;
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule, FormsModule,RouterTestingModule],
      declarations: [ LoginComponent ],
      providers: [ServiceService]
    })  
    .compileComponents();
    serviceService = TestBed.inject(ServiceService);
    spyOn(serviceService,'loginUserFromRemote').and.returnValue(of(''));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
 

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // test to check ngOnInit method existence
  it('ngOnInit() should exists', () => {
    expect(component.ngOnInit).toBeTruthy();
  });

   // test to check loginUser method existence
   it('loginUser() should exists', () => {
    expect(component.loginUser).toBeTruthy();
  });

  // test to check loginUser without credentials
  it('loginUser() should return error message', () => {
     component.loginUser();
    expect(component.msg).toEqual('login failed');
});
// test to check loginUser with one credentials
it('loginUser() should return error message for email only', () => {
  component.user.emailId='test@gmail.com';
  component.loginUser();
 expect(component.msg).toEqual('login failed');
});

    // test to check loginUser is verifying form is valid or not
  it('loginUser() should return login success', () => {
      component.user.emailId ='abc@gmail.com';
      component.user.password='abcd1';
      component.loginUser();
      expect(component.msg).toEqual('Login success');
  });

   
  
   
});