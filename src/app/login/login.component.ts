
import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../Service/service.service';

import { User } from '../user';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user= new User();
  msg='';
  userid="";
  imgUrl="https://i.pinimg.com/736x/41/71/44/4171449ab62bcdc64872ed4b0b9edf64.jpg";
  username="";
  constructor( private _service: ServiceService, private _router : Router) { }

  ngOnInit(){
  }

  loginUser(){

  
    if(this.user.emailId==null || this.user.password==null){
      console.log("null");
      console.log("No data!!");
      this.msg="login failed";
      alert("Please enter Your details!")
      return
    }

   console.log(this.user);
     this._service.loginUserFromRemote(this.user).subscribe(
      data =>{ 
         this.msg= "Login success";
        console.log(this.user);

         console.log("response recieved");
        
       alert("Login success")  
        
       this.userid=this.user.emailId;
        this.username=this.user.username;
        this._service.setUser(this.userid);
        
         this._router.navigate(['/home']);
    
       },
      error =>{
        this.msg= "Invalid Credentials, Please enter Email and Password"
        alert(this.msg)
        console.log("Exeption Occuerd");
      });
      
  }
 

}
